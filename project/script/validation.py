
import numpy as np
import matplotlib.pyplot as plt
from argparse import ArgumentParser

from WetBulb import WetBulb, calc_RH_from_T_Td

def Characteristic_Dimension(size):
	if size == 's':
		return 0.1
	elif size == 'm':
		return 0.2
	elif size == 'l':
		return 0.3
	elif size == 'xl':
		return 0.4
	elif size == '2l':
		return 0.5
	elif size == '3l':
		return 0.6

def Specific_Mass(num_clothes, type='sarja'):
	if type == 'knit': # for sport shirts
		return 0.22 * num_clothes
	elif type == 'sarja': # for uniform and jeans trousers
		return 0.42 * num_clothes

def Energy_Activation(T):
	# iteratively adjust and save per user
	# preset: low temp: -2.2816831786287124e-06 * 10
	# preset: high temp: -2.2816831786287124e-06 * 5
	if T < 25:
		return -2.2816831786287124e-06 * 10
	else:
		return -2.2816831786287124e-06 * 5

def Nc(T=18.0, RH=40, num_clothes=10, size='l', type='sarja'):
	d_SL = Characteristic_Dimension(size) # corresponds to cloth size (m)
	v_air = 0.5
	p_ss = Specific_Mass(num_clothes, type) # corresponds to cloth knits, multiply by num_clothes
	T_vap = T # dry bulb / air
	T_air = WetBulb(np.array([T_vap]), np.array([101325]), np.array([RH]), HumidityMode=1)[0][0] # dry bulb, atmosphere, RH
	E = Energy_Activation(T_vap) # corresponds to temperature, low temp-high activation
	Nc = 5.63e-05 * np.power(d_SL * v_air, 0.42) / (2400 * p_ss * E) * (T_air - T_vap)

	return Nc, E

def t_Ad(t, X0, config=None):
	nc, e = Nc(T=config['T'], RH=config['RH'], num_clothes=config['num_clothes'], size=config['size'], type=config['type'])
	return nc * t / X0, e

def GDC(t_Ad, type='sarja'):
	# knit actually has a slower slope, but better at Nc and X0
	if type == 'knit':
		a = 2.23
		b = 1.45
	elif type == 'sarja':
		a = 2.88
		b = 1.20
	
	return np.exp(-a * np.power(t_Ad, b))

def Iterative_FindTime(config, max_time=48):
	_t_Ad, e = t_Ad(np.linspace(0, max_time * 60, max_time), X0, config)
	for t in range(max_time):
		if GDC(_t_Ad[t], config['type']) < 0.2:
			return t, e
	
	return -1, e

def Iterative_AdjustEnergy(t, config):
	# TODO: Iterative adjust E, if user_t > pred_t, raise E (by scale), else lower E
	if Iterative_FindTime(config) < t:
		return

if __name__ == '__main__':
	parser = ArgumentParser()
	parser.add_argument("-T", dest='T')
	parser.add_argument("-RH", dest='RH')
	parser.add_argument("-n", dest='num_clothes')
	parser.add_argument("-s", dest='size')
	parser.add_argument("-type", dest='type')
	args = parser.parse_args()

	X0 = 0.5 # d.b., varies with different cloth type, assume 0.5 for sarja, 0.25 for knit
	config = {
		'T': 18.0,
		'RH': 80,
		'num_clothes': 10,
		'size': 'l',
		'type': 'sarja'
	}

	config['T'] = float(args.T)
	config['RH'] = int(args.RH)
	config['num_clothes'] = int(args.num_clothes)
	config['size'] = args.size
	config['type'] = args.type

	time, e = Iterative_FindTime(config)
	print(time)
	print(e)
	# hr = 12
	# t_Ad = t_Ad(np.linspace(0, hr * 60), X0, config)

	# # moisture ratio / time (mins)
	# plt.plot(np.linspace(0, hr * 60), GDC(t_Ad, config['type']))
	# plt.show()

	