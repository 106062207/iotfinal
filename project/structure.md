# Structure

```
-------------------
    index.js: main entrance of server
-------------------
    route.js: handle routes for operation
    user.js: functions about user account
    data.js: functions about data
    script.js: function that run script
-------------------
    /model
        user.js: model in db of user info
        data.js: model in db of data info
-------------------
    /script
        validation.py
        WetBulb.py
-------------------
    /client
-------------------
        /public
            index.html: main html file of client
-------------------
        /src
            index.js: main entrance of react
            navbar.js: navbar of each page
            home.js: home page
            account.js: sign in/ sign up page
            data.js: page that shows more detail data
```
