// import
const express = require('express');
const User = require('./model/user');

module.exports = {
    // user signup
    SignUp: async (username, email, password) => {
        try{
            //find if user is already signed up
            let user = await User.findOne({
                email: email,
            });
    
            if (user) {
                return JSON.stringify({err: 'User Already Exists'});
            }
            else {
                // save user data to database
                let user = new User({
                    username: username,
                    email: email,
                    password: password,
                });
                
                await user.save((err) => {
                    if(err) {
                        console.error(err);
                    }
                });
        
                return JSON.stringify({
                    signin: true,
                    user: {
                        username: user.username,
                        email: user.email,
                        password: user.password,
                    },
                });
            }
        }
        catch (err) {
            return err;
        }
    },

    SignIn: async(email, password) => {
        try {
            // find if user is already signed up
            let user = await User.findOne({
                email: email
            });
    
            if (!user) {
                return JSON.stringify({err: 'User Not Exists'});
            }
            else {
                // further check if password is correct
                if(password !== user.password) {
                    return JSON.stringify({err: 'Incorrect Password'});
                }
        
                return JSON.stringify({
                    signin: true,
                    user: {
                        username: user.username,
                        email: user.email,
                        password: user.password,
                    },
                });
            }
        }
        catch (err) {
            return err;
        }
    }
}