// import
const express = require('express');
const app = express();
const {spawn} = require('child_process');
const User = require('./model/user');

app.use(express.json());

module.exports = {
    GetTime: async (temp, humi, num, size, type) => {
        try {
            //execute command
            let proc = await spawn(
                // command
                'python', 
                //args
                [
                    './script/validation.py', 
                    '-T',
                    temp,
                    '-RH',
                    humi,
                    '-n',
                    num,
                    '-s',
                    size,
                    '-type',
                    type
                ]
            );

            let data = '';
            
            proc.stderr.on('data', (data) => {
                console.error(`stderr: ${data}`);
            });
            
            proc.on('close', (code) => {
                console.log(`child process exit code: ${code}`);
            });

            for await (const d of proc.stdout) {
                data = d.toString().split('\r\n');
                console.log(`stdout: ${d}`);
            }

            return data;
        }
        catch (err) {
            console.error(err);
        }
    },
    AdjustEnergy: async (temp, humi, num, size, type, time) => {
        try {
            //execute command
            let proc = await spawn(
                // command
                'python', 
                //args
                [
                    './script/validation2.py', 
                    '-T',
                    temp,
                    '-RH',
                    humi,
                    '-n',
                    num,
                    '-s',
                    size,
                    '-type',
                    type,
                    '-u',
                    time,
                ]
            );

            let data = '';
            
            proc.stderr.on('data', (data) => {
                console.error(`stderr: ${data}`);
            });
            
            proc.on('close', (code) => {
                console.log(`child process exit code: ${code}`);
            });

            for await (const d of proc.stdout) {
                data = d.toString().split('\r\n');
                console.log(`stdout: ${d}`);
            }

            return data;
        }
        catch (err) {
            console.error(err);
        }
    },
}

