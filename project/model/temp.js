const mongoose = require('mongoose');

const TempSchema = new mongoose.Schema({
    //data structure
    temp: {
        type: String,
        required: true
    },
    date: {
        type: String,
        required: true,
    }
});

const Temp = mongoose.model('Temp', TempSchema, 'temps');

module.exports = Temp;