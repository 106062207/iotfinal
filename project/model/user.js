const mongoose = require('mongoose');

const UserSchema = new mongoose.Schema({
    //data structure
    username: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    date: {
        type: String,
        default: Date.now().toString(),
    }
});

const User = mongoose.model('User', UserSchema, 'users');

module.exports = User;