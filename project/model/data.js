const mongoose = require('mongoose');

const DataSchema = new mongoose.Schema({
    //data structure
    email: {
        type: String,
        required: true
    },
    temp: {
        type: String,
        required: true
    },
    humi: {
        type: String,
        required: true
    },
    num: {
        type: String,
        required: true
    },
    size: {
        type: String,
        required: true
    },
    type: {
        type: String,
        required: true
    },
    predictTime: {
        type: String,
        required: true
    },
    actualTime: {
        type: String,
        default: '0'
    },
    energy: {
        type: String,
        required: true
    },
    date: {
        year: String,
        month: String,
        day: String,
        date: String,
        hour: String,
        minute: String,
    }
});

const Data = mongoose.model('Data', DataSchema, 'datas');

module.exports = Data;