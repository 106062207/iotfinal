const mongoose = require('mongoose');

const HumiSchema = new mongoose.Schema({
    //data structure
    humi: {
        type: String,
        required: true
    },
    date: {
        type: String,
        required: true,
    }
});

const humi = mongoose.model('humi', HumiSchema, 'humis');

module.exports = humi;