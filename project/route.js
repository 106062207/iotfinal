// import
const express = require('express');
const router = express.Router();
module.exports = router;
const User = require('./user');
const Data = require('./data');
const Script = require('./script');

// to parse or make json
router.use(express.json());

// post that request for signing up 
router.post('/signup', async (req, res) => {
    try {
        // get request data
        const {
            username,
            email,
            password
        } = req.body;

        // get response of signing up (and then signing in)
        const msg = await User.SignUp(username, email, password);
        res.send(msg);
    }
    catch (err) {
        res.send(err);
    }
});

// post that request for signing in 
router.post('/signin', async (req, res) => {
    try{
        // get request data
        const {
            email, 
            password
        } = req.body;

        // get response of signing in
        const msg = await User.SignIn(email, password);
        res.send(msg);
    }
    catch (err) {
        res.send(err);
    } 
});

// post that request for starting drying process
router.post('/start', async (req, res) => {
    try{
        // get request data
        const {
            email,
            numOfClothes,
            sizeOfClothes,
            typeOfClothes,
        } = req.body;

        // Todo: get temp and humi
        let temp = await Data.FindTemp();
        let humi = await Data.FindHumi();

        // run script and get predict time and energy
        const out = await Script.GetTime(temp, humi, numOfClothes, sizeOfClothes, typeOfClothes);

        // save data to database
        const msg = await Data.SaveData(email, temp, humi, numOfClothes, sizeOfClothes, typeOfClothes, out[0], out[1]);
        if(msg.err) {
            console.error(msg.err);
            return;
        }
        // send data to client (mainly predict time)
        res.json({
            temp: temp,
            humi: humi,
            predictTime: out[0],
            date: msg.date,
        });
    }
    catch (err) {
        res.send(err);
    } 
});

// post that request for exiting drying process
router.post('/exit', async (req, res) => {
    try {
        // get request data
        const {
            email,
            date,
        } = req.body;

        let end = new Date();
        let start = new Date(
            parseInt(date.year),
            parseInt(date.month),
            parseInt(date.date),
            parseInt(date.hour),
            parseInt(date.minute),
        );
        let actualTime = Math.abs(end - start) / 36e5;
        console.log(actualTime);

        // save exit data
        const msg = await Data.Exit(email, date, actualTime);
        console.log(msg);

        // adjust energy
        const msg2 = await Script.AdjustEnergy(msg.temp, msg.humi, msg.num, msg.size, msg.type, actualTime);
        console.log(msg2);

        res.json(msg2);
    }
    catch (err) {
        res.send(err);
    }
})

// post that request for getting history data by specifying user and time range
router.post('/getdata', async (req, res) => {
    try {
        // get request data
        const {
            email,
            type
        } = req.body;

        // find data in database
        const msg = await Data.FindData(email, type);
        // return data (error message if error encountered)
        res.json(msg);
    }
    catch (err) {
        res.send(err);
    }
});

router.post('/getEnergy', async (req, res) => {
    // get request data
    const {
        temp
    } = req.body;
    
    // get according energy by temp
    const energy = await Data.GetEnergy(temp);
    res.send(energy);
});

router.post('/updateEnergy', async (req, res) => {
    // get request data
    const {
        temp,
        energy
    } = req.body;
    
    // update according energy by temp
    await Data.UpdateEnergy(temp, energy);
    res.send('ok');
});

// generate random number(integer) in specified range for fake data
function randomNumber(min, max) {  
    return Math.round(Math.random() * (max - min) + min); 
} 

// generate fake data
router.get('/savedata', async (req, res) => {
    try{
        const sizes = ['s', 'm', 'l', 'xl', '2l', '3l'];
        const types = ['knit', 'sarja'];
        let msg = '';

        for (let i = 1; i <= 31; i++) {
            let temp = randomNumber(5, 35).toString();
            let humi = randomNumber(10, 90).toString();
            let num = randomNumber(10, 20).toString();
            let size = sizes[randomNumber(0, 5)];
            let type = types[randomNumber(0, 1)];
            let out = await Script.GetTime(temp, humi, num, size, type);
            msg = await Data.SaveData('456', temp, humi, num, size, type, out[0], out[1],
                (parseInt(out[0]) + randomNumber(-5, 5)).toString(),
                {
                    year: '2020',
                    month: '11',
                    day: ((i % 7 + 1) % 7).toString(),
                    date: i.toString(),
                    hour: '8',
                    minute: '0',
                }
            );
        }
        
        // console.log(msg.data);
        res.json(msg);
        // res.send('finish');
    }
    catch (err) {
        res.send(err);
    } 
});

router.post('/savetemp', async (req, res) => {
    const {
        temp
    } = req.body;
    await Data.SaveTemp(temp);
    res.send();
});

router.post('/savehumi', async (req, res) => {
    const {
        humi
    } = req.body;
    await Data.SaveTemp(humi);
    res.send();
});

router.get('/test', async (req, res) => {
    let out = await Data.FindHumi();
    res.send(out);
});

// get current temperature and humidity and time at home page
router.get('/', async (req, res) => {
    // Todo: get temp and humi
    let temp = await Data.FindTemp();
    let humi = await Data.FindHumi();

    // get predict time with default variables
    const out = await Script.GetTime(temp, humi, '10', 'l', 'knit');
    res.json({temp: temp, humi: humi, predictTime: out[0]});
});