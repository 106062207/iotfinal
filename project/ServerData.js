// import
const express = require('express');
const app = express();
const {spawnSync} = require('child_process');
const { SaveTemp, SaveHumi } = require('./data');
module.exports = {
    DataGet:async()=>{
        setInterval(function(){
            let proc = spawnSync(
                // command
                'python', 
                //args
                [
                    './request.py'
                ]
            );

            var fs = require('fs');
            var T, H;
            var config;
 
            fs.readFile('./requestT.json', function (err, configT) {
                if (err) throw err;
                
                config = JSON.parse(configT);      
                T = config.dataChannels[0].dataPoints[0].values.value;
                //console.log(T);
                if(T)SaveTemp(T);
            });

            

            fs.readFile('./requestH.json', function (err, configH) {
                if (err) throw err;
            
                config = JSON.parse(configH);
                H = config.dataChannels[0].dataPoints[0].values.value;
                //console.log(H);
                if(H)SaveHumi(Math.round(H));
            }); 
            
            

            /*var configT = require('./requestT.json');
            var configH = require('./requestH.json');

            
            var H = configH.dataChannels[0].dataPoints[0].values.value;*/

            //console.log(T, H);
        }, 6000)
        //return output;
    }
}