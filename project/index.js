// import
const express = require('express');
const app = express();
module.exports = app;
const port = process.env.PORT || 9000;
const cors = require('cors');
const mongoose = require('mongoose');
const ServerData = require('./ServerData');

// set up default mongoose connection
mongoose.connect('mongodb://localhost:27017/db', { useUnifiedTopology: true, useNewUrlParser: true });

mongoose.Promise = global.Promise;

const db = mongoose.connection;
// bind connection to error event (to get notification of connection errors)
db.on('error', console.error.bind(console, 'MongoDB connection error:'));
// send msg once db is open
db.once('open', () => {console.log('open db')});

// enable all cors requests
app.use(cors());

// route
app.use('/', require('./route'));

ServerData.DataGet();

if (!module.parent) {
  app.listen(port);
}

