# Read Me

## Server

- open project folder

- **First Time**
  - make sure node.js, mongodb, python 3 is installed
    - mongodb download link: https://www.mongodb.com/try/download/community
    - pip install numpy, matplotlib to execute python script
  - command npm install: download all packages needed

- command node index.js to run the server

## Client

- open client folder

- **First Time**
  - make sure node.js is installed
  - command npm install: download all packages needed

- command npm start to run the client

- ![homepage](./images/homepage.png)
- click the menu button on top-right
- ![homepage with open menu](./images/home_menu.png)
- click sign in button
- ![sign in page](./images/signin.png)
- if first time
  - ![sign up page](./images/signup.png)
  - fill in username, email and password
  - press the sign up button to sign up as new user
- if already signed up as an user
  - fill in email and password
  - press the sign in button to sign in

- ![homepage after signed in](./images/home_signin.png)
- press the start button to start the drying process
- ![homepage with popover form](./images/home_form.png)
- fill in informations respectively, and then press the submit button to start
- ![homepage in process](./images/home_process.png)
- press the exit button after the process is completed
- ![homepage after signed in with open menu](./images/home_signin_menu.png)
- press the data button to view the history data
- ![data page](./images/data.png)
- press different timescale button to view data