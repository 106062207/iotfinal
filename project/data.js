// import
const express = require('express');
const Data = require('./model/data');
const User = require('./model/user');
const Temp = require('./model/temp');
const Humi = require('./model/humi');

module.exports = {
    // insert a new process into database
    SaveData: async (email, temp, humi, num, size, type, predictTime, energy,  actualTime = '0', fake = null) => {
        try {
            let user = await User.findOne({
                email: email,
            });
    
            if (!user) {
                return JSON.stringify({err: 'User Not Exists'});
            }
            else {
                let date = new Date();
                let data = new Data({
                    email: email,
                    temp: temp,
                    humi: humi,
                    num: num,
                    size: size,
                    type: type,
                    predictTime: predictTime,
                    actualTime: actualTime,
                    energy: energy,
                    date: (!fake)? {
                        year: date.getFullYear().toString(),
                        month: (date.getMonth()).toString(),
                        day: date.getDay().toString(),
                        date: date.getDate().toString(),
                        hour: date.getHours().toString(),
                        minute: date.getMinutes().toString(),
                    }: fake,
                    
                });
                
                await data.save((err) => {
                    if(err) {
                        console.error(err);
                    }
                });
                return {date: (!fake)? {
                        year: date.getFullYear().toString(),
                        month: (date.getMonth()).toString(),
                        day: date.getDay().toString(),
                        date: date.getDate().toString(),
                        hour: date.getHours().toString(),
                        minute: date.getMinutes().toString(),
                    }: fake,
                };
            }
        }
        catch (err) {
            return err;
        }
    },

    // get history data with specified user and timescale
    FindData: async (email, type) => {
        try {
            let user = await User.findOne({
                email: email
            });
    
            if (!user) {
                return JSON.stringify({err: 'User Not Exists'});
            }
            else {
                let condition = {};
                let date = new Date();
                date.setFullYear(2020, 11, 11);

                console.log('Find data by timescale type: ' + type);

                if(type === 'week') {
                    let week = [];
                    let currentDate = date.getDate();
                    let currentDay = date.getDay();
                    for(let i = 0; i < 7; i++) {
                        
                        let diff = currentDate - currentDay + i;
                        let weekday = new Date(date.setDate(diff)).getDate();
                        week.push(weekday.toString());
                    }
                    condition = {
                        email: email,
                        'date.year': date.getFullYear().toString(),
                        'date.month': (date.getMonth()).toString(),
                        'date.date': {$in: week},
                    };
                }
                else if (type === 'month') {
                    condition = {
                        email: email,
                        'date.year': date.getFullYear().toString(),
                        'date.month': (date.getMonth()).toString(),
                    };
                }
                else if (type === 'year') {
                    condition = {
                        email: email,
                        'date.year': date.getFullYear().toString(),
                    };
                }
                else {
                    return JSON.stringify({err: 'Type Not Exists'});
                }
                let data = await Data.find(condition).lean();

                if (!data) {
                    return JSON.stringify({err: 'Data Not Exists'});
                }

                return data;
            }
        }
        catch (err) {
            return err;
        } 
    },

    SaveTemp: async (temp) => {
        let tempData = new Temp({
            temp: temp,
            date: Date.now().toString()
        });

        await tempData.save((err) => {
            if(err) {
                console.error(err);
            }
        });
    },

    FindTemp: async () => {
        let temp = await Temp.findOne({},{},{sort:{'date': -1}}, (err, doc) => {
            if(err) {
                console.error(err);
            }

            console.log(doc);
        });

        return temp.temp;
    },

    SaveHumi: async (humi) => {
        let humiData = new Humi({
            humi: humi,
            date: Date.now().toString(),
        });

        await humiData.save((err) => {
            if(err) {
                console.error(err);
            }
        });
    },

    FindHumi: async () => {
        let humi = await Humi.findOne({},{},{sort:{'date': -1}}, (err, doc) => {
            if(err) {
                console.error(err);
            }
            console.log(doc);
        });

        return humi.humi;
    },

    // add actual drying time while exiting
    Exit: async (email, start, actualTime) => {
        let user = await User.findOne({
            email: email
        });

        if (!user) {
            return JSON.stringify({err: 'User Not Exists'});
        }
        else {
            let data = await Data.findOneAndUpdate({
                email: email,
                date: start,
            }, {actualTime: actualTime}, (err, raw) => {
                if(err) {
                    console.error(err);
                    return;
                }
                console.log(raw);
            }).lean();

            console.log(data);

            return data;
        }
    },

    GetEnergy: async (temp) => {
        let data = await Data.findOne({
            temp: temp,
        }, (err, res) => {
            console.error(err);
        }).lean();

        if (!data) {
            if (parseFloat(temp) < 25.0)
                return (-2.2816831786287124e-06 * 10).toString();
            else
                return (-2.2816831786287124e-06 * 5).toString();
        }

        return data.energy;
    },

    UpdateEnergy: async (temp, energy) => {
        await Data.updateMany({
            temp: temp,
        }, {energy: energy}, (err, raw) => {
            if(err) {
                console.error(err);
                return;
            }
            console.log(raw);
        });

        console.log('energy update success');
    },

    // remove a document (test)
    Remove: () => {
        Data.deleteOne({name: 'hi'}).exec(() => {
            // callback
            console.log('deleted');
        });
    }
}



