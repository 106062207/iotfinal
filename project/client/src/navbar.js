import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import * as UI from '@material-ui/core/';
import * as Icon from '@material-ui/icons';
import {useHistory} from 'react-router-dom';

// define styles of navbar
const navbarStyle = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    title: {
        flexGrow: 1,
        textAlign: 'center',
        color: 'white',
        marginRight: -16,
    },
    menu: {
        color: 'white',
    },
    menuIcon: {
        marginRight: theme.spacing(2),
    }
}));

// navbar
export default function Navbar(props) {
    const style = navbarStyle();
    const [menu, setMenu] = React.useState(null);
    const isMenuOpen = Boolean(menu);
    const history = useHistory();

    // close menu
    const CloseMenu = () => {
        setMenu(null);
    };

    // callback on menu button clicked
    const OnMenuButtonClicked = (event) => {
        setMenu(event.currentTarget);
    };

    // callback on home button in menu clicked, redirect to homepage if currently not
    const OnHomeButtonClicked = (event) => {
        CloseMenu();

        // reset data to force anim update
        if (props.route !== '/') {
            props.ResetData();
            props.UpdateRoute('/');
        }
            
        // redirect to home page
        history.push('/');
    }

    // callback on Account button in menu clicked, redirect to Account page if currently not, only available when not signing in
    const OnAccountButtonClicked = (event) => {
        CloseMenu();
        if (props.route !== '/account') {
            props.UpdateRoute('/account');
        }
        // redirect to account page
        history.push('/account');
    }

    // callback on Data button in menu clicked, redirect to Data page if currently not, only available when signed in
    const OnDataButtonClicked = (event) => {
        CloseMenu();
        // reset data to force anim update
        if (props.route !== '/data') {
            props.ResetData();
            props.UpdateRoute('/data');
        }
        // redirect to data page
        history.push('/data');
    }

    // popover menu
    const Menu = (
        <UI.Menu
         anchorEl={menu}
         anchorOrigin={{vertical: 'top', horizontal: 'right'}}
         keepMounted
         transformOrigin={{vertical: 'top', horizontal: 'right'}}
         open={isMenuOpen}
         onClose={CloseMenu}
        >
            {/*Menu Button*/}
            <UI.MenuItem>
                <UI.IconButton size='small' onClick={OnHomeButtonClicked}>
                    <Icon.Home className={style.menuIcon} />
                    <p>Home</p>
                </UI.IconButton>
            </UI.MenuItem>
            {!props.user.signin && 
                <UI.MenuItem>
                    <UI.IconButton size='small' onClick={OnAccountButtonClicked}>
                        <Icon.AccountCircle className={style.menuIcon} />
                        <p>Sign In</p>
                    </UI.IconButton>
                </UI.MenuItem>}
            {props.user.signin && 
                <UI.MenuItem>
                    <UI.IconButton size='small' onClick={OnDataButtonClicked}>
                        <Icon.InsertChart className={style.menuIcon} />
                        <p>Data</p>
                    </UI.IconButton>
                </UI.MenuItem>}
        </UI.Menu>
    );

    return (
        <div className={style.root}>
            <UI.AppBar position='static'>
                {/* inline bar */}
                <UI.Toolbar>
                    {/* title */}
                    <UI.Typography variant='h6' className={style.title}>
                        Product Name
                    </UI.Typography>
                    {/* menu icon button */}
                    <UI.IconButton edge='end' onClick={OnMenuButtonClicked}>
                        <Icon.Menu className={style.menu}/>
                    </UI.IconButton>
                </UI.Toolbar>
            </UI.AppBar>
            {Menu}
        </div>
    );
};