import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import * as UI from '@material-ui/core/';
import {ResponsiveContainer, PieChart, Pie, Cell} from 'recharts';

// define styles of piechart
const circlechartStyle = makeStyles((theme) => ({
    circlechart: {
        marginTop: '2em',
    },
    desc: {
        textAlign: 'center',
        color: '#514BBA',
    },
}));

// piechart
function CircleChart(props) {
    const style = circlechartStyle();
    // color thresholded by predict time
    let color = (props.predictTime > 24.0)? '#f79123':'#2ffa20';
    // grey part of piechart
    let foo = 48 - props.predictTime;
    
    // data to draw in chart
    let arr = [{name: 'predictTime', color: color, value: props.predictTime}, {name: 'foo', color: '#a6a6a6', value: foo}];

    return (
        <div>
            <ResponsiveContainer height={350}  className={style.circlechart}>
                <PieChart>
                    <Pie 
                        data={arr.slice()} dataKey='value' nameKey='name'
                        cx='50%' cy='50%' outerRadius={140} innerRadius={100} fill='#000000'
                        labelLine={false}
                        label={({cx, cy, midAngle, innerRadius, outerRadius, percent, index,}) => {
                            if (index === 0) {
                                return (    
                                    <text x={cx} y={cy} fill='#514BBA' textAnchor='middle' dominantBaseline='central'>
                                        Estimated Time: {props.predictTime.toString()+'hr'}
                                    </text>
                                );
                            };
                            return;
                        }}
                    >
                        {
                            arr.map((entry, index) => <Cell key={index} fill={entry.color}/>)
                        }
                    </Pie>
                </PieChart>
            </ResponsiveContainer>
            <UI.Typography variant='body1' className={style.desc}>
                {'Temp: ' + props.temp.toString() + '\xb0c , Humi: ' + props.humi.toString() + '%'}
            </UI.Typography>
        </div>
    )
}

// style of input form to start drying process
const InputFormStyle = makeStyles((theme) => ({
    actiondiv: {
        position: 'relative',
    },
    progress: {
        position: 'absolute',
        top: '50%',
        right: '50%',
        marginTop: -12,
        marginRight: -12,
    }
}));

// input form to start drying process
function InputForm(props) {
    const style = InputFormStyle();
    const [numOfClothes, setNumOfClothes] = React.useState(10);
    const [sizeOfClothes, setSizeOfClothes] = React.useState('s');
    const sizeOfClothesArr = [
        {
            value: 's',
            label: 's',
        },
        {
            value: 'm',
            label: 'm',
        },
        {
            value: 'l',
            label: 'l',
        },
        {
            value: 'xl',
            label: 'xl',
        },
        {
            value: '2l',
            label: '2l',
        },
        {
            value: '3l',
            label: '3l',
        },
    ];

    const [typeOfClothes, setTypeOfClothes] = React.useState('knit');
    const typeOfClothesArr = [
        {
            value: 'knit',
            label: 'knit',
        },
        {
            value: 'sarja',
            label: 'sarja',
        }
    ];

    const [loading, setLoading] = React.useState(false);

    // callback on submit button of input form clicked
    const OnSubmitButtonClicked = async () => {
        setLoading(true);
        // for re-rendering
        props.ResetData();
        let temp, humi, predictTime, date;
        let actualTime = props.data.actualTime.current;

        // post to server for getting predict time
        const url = 'http://localhost:9000/start';
        await fetch(url, {
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: props.email,
                numOfClothes: numOfClothes.toString(),
                sizeOfClothes: sizeOfClothes,
                typeOfClothes: typeOfClothes,
            }),
            cache: 'no-cache',
            method: 'POST',
            mode: 'cors',
            redirect: 'manual'
        }).then((res) => {
            if(res.ok) {
                return res.json();
            }
        }).then((json) => {
            if(json.err)
            {
                alert(json.err);
                return;
            }
            temp = parseFloat(json.temp);
            humi = parseInt(json.humi);
            predictTime = parseInt(json.predictTime);
            date = json.date;
            setLoading(false);
            props.HandleInputFormClose();
        }).catch((err) => {
            console.error(err);
        });

        // update client data
        props.UpdateCurrentData(temp, humi, predictTime, actualTime, date);
    };

    // update value of NumOfClothes
    const OnNumOfClothesChange = (event) => {
        setNumOfClothes(event.target.value);
    }

    // update value of SizeOfClothes
    const OnSizeOfClothesChange = (event) => {
        setSizeOfClothes(event.target.value);
    }

    // update value of TypeOfClothes
    const OnTypeOfClothesChange = (event) => {
        setTypeOfClothes(event.target.value);
    }

    return (
        <UI.Dialog
            open={props.openForm}
            onClose={props.HandleInputFormClose}
        >
            <UI.DialogTitle>Input Form</UI.DialogTitle>
            <UI.DialogContent>
                <div>
                    <UI.TextField
                        type='number'
                        fullWidth
                        label='Number Of Clothes'
                        helperText='Some important text'
                        value={numOfClothes}
                        onChange={OnNumOfClothesChange}
                    />
                </div>
                <div>
                    <UI.TextField
                        select
                        fullWidth
                        label='Size Of Clothes'
                        helperText='Some important text'
                        value={sizeOfClothes}
                        onChange={OnSizeOfClothesChange}
                    >
                        {sizeOfClothesArr.map((option) => (
                            <UI.MenuItem
                                key={option.value}
                                value={option.value}
                            >
                                {option.label}
                            </UI.MenuItem>
                        ))}
                    </UI.TextField>
                </div>
                <div>
                    <UI.TextField
                        select
                        fullWidth
                        label='Type Of Clothes'
                        helperText='Some important text'
                        value={typeOfClothes}
                        onChange={OnTypeOfClothesChange}
                    >
                        {typeOfClothesArr.map((option) => (
                            <UI.MenuItem
                                key={option.value}
                                value={option.value}
                            >
                                {option.label}
                            </UI.MenuItem>
                        ))}
                    </UI.TextField>
                </div>
            </UI.DialogContent>
            <UI.DialogActions>
                <div  className={style.actiondiv}>
                    <UI.Button onClick={props.HandleInputFormClose} color="primary">
                        Cancel
                    </UI.Button>
                </div>
                <div  className={style.actiondiv}>
                    <UI.Button onClick={OnSubmitButtonClicked} variant='contained' color="primary" disabled={loading}>
                        Submit
                    </UI.Button>
                    {loading && <UI.CircularProgress size={24} className={style.progress} />}
                </div>
            </UI.DialogActions>
        </UI.Dialog>
    )
}

// define styles of content
const contentStyle = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    adviceDivWrapper: {
        textAlign: 'center',
        paddingTop: '1.5em'
    },
    adviceDiv: {
        textAlign: 'center',
        margin: '0 auto',
        width: '80%'
    },
    adviceText: {
        textAlign: 'left',
        
    },
    switchbuttonDiv: {
        paddingTop: '2em',
        textAlign: 'center',
    }
}));

// content
function Content(props) {
    const style = contentStyle();
    const SwitchButtonText = props.start? 'Exit Process': 'Get Started';
    const [openForm, setOpenForm] = React.useState(false);

    // callback on clicking switch button
    const OnSwitchButtonClicked = async () => {
        if(!props.start) {
            setOpenForm(true);
        }
        else {
            let temp = props.data.temp.current;
            let humi = props.data.humi.current;
            let predictTime = props.data.predictTime.current;
            props.ResetData();
            const url = 'http://localhost:9000/exit';
            await fetch(url, {
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    email: props.user.email,
                    date: props.data.date.current,
                }),
                cache: 'no-cache',
                method: 'POST',
                mode: 'cors',
                redirect: 'manual'
            }).then((res) => {
                if(res.ok) {
                    return res.json();
                }
            }).then((json) => {
                if(json.err)
                {
                    alert(json.err);
                    return;
                }
                console.log(json);
            }).catch((err) => {
                console.error(err);
            });

            props.UpdateCurrentData(temp, humi, predictTime, '0', {});
        }
        props.UpdateStart();
    };

    // update value of openForm
    const HandleInputFormClose = () => {
        setOpenForm(false);
    }

    // advice text depending on predict time
    const advice = (props.data.predictTime.current < 24)? 
        'the sunshine is streaming through the window. Let\'s seize the time and do some laundry':
        'rain and clouds are on the way. Let\'s dry the washing inside or try tomorrow';

    // whole advice text depending on predict time
    const adviceText = (
        <div className={style.adviceDivWrapper}>
            <div className={style.adviceDiv}>
                {/* greeting text*/}
                <UI.Typography variant='h5' className={style.adviceText}>
                    Hi {props.user.username},
                </UI.Typography>
                {/* advice text depending on predict time*/}
                <UI.Typography variant='h5' className={style.adviceText}>
                    {advice}
                </UI.Typography>
            </div>
        </div>
    );

    return (
        <div className={style.root}>
            {/* advice*/}
            {adviceText}

            {/* circle chart time*/}
            <CircleChart 
                temp={props.data.temp.current}
                humi={props.data.humi.current}
                predictTime={props.data.predictTime.current}
            />

            {/* Start button */}
            {props.user.signin && <div className={style.switchbuttonDiv}>
                <UI.Button variant='contained' color='primary' size='large' onClick={OnSwitchButtonClicked}>
                    {SwitchButtonText}
                </UI.Button>
                {/* Input Form for drying process*/}
                <InputForm 
                    openForm={openForm} 
                    HandleInputFormClose={HandleInputFormClose} 
                    email={props.user.email}
                    data={props.data}
                    UpdateCurrentData={props.UpdateCurrentData}
                    ResetData={props.ResetData}
                />
            </div>}
        </div>
    );
};

//  home page
export default class Home extends React.Component {
    // execute when home component is mounted
    async componentDidMount() {
        await this.GetData();
    }

    // get data from server and update
    async GetData() {
        const url = 'http://localhost:9000/';
        await fetch(url, {method: 'GET'}).then((res) => {
            if(res.ok) {
                return res.json();
            }
        }).then((json) => {
            let temp = parseFloat(json.temp);
            let humi = parseInt(json.humi);
            let predictTime = parseInt(json.predictTime);
            let actualTime = this.props.data.actualTime.current;
            let date = this.props.data.date.current;
            this.props.UpdateCurrentData(temp, humi, predictTime, actualTime, date);
        }).catch((err) => {
            console.error(err);
        });
    }

    render() {
        return (
            <div>
                <Content
                    route={this.props.route}
                    user={this.props.user}
                    data={this.props.data}
                    start={this.props.start}

                    UpdateRoute={this.props.UpdateRoute}
                    UpdateCurrentData={this.props.UpdateCurrentData}
                    UpdateStart={this.props.UpdateStart}
                    ResetData={this.props.ResetData}
                />
            </div>
        );
    }
}