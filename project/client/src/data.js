import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import * as UI from '@material-ui/core/';
import {ResponsiveContainer, XAxis, YAxis, Tooltip, Legend, CartesianGrid, ScatterChart, Scatter, ZAxis, LineChart, Line} from 'recharts';

// week enum for chart label
let WeekEnum = [
    'Sun',
    'Mon',
    'Tue',
    'Wed',
    'Thu',
    'Fri',
    'Sat',
];

// week enum for chart label
let MonthEnum = [
    'Jan',
    'Feb',
    'Mar',
    'Apr',
    'May',
    'Jun',
    'Jul',
    'Aug',
    'Sep',
    'Oct',
    'Nov',
    'Dec',
];

// define styles of linechart
const historychartStyle = makeStyles((theme) => ({
    root: {
        textAlign: 'center',
        marginTop: '1.2em',
    },
    container: {
        marginTop: '1.2em',
    },
    desc: {
        textAlign: 'center',
    },
}));

// get date for chart label
function GetDate(date, timeScale) {
    if (timeScale === 'week') {
        return WeekEnum[date.day];
    }
    else if (timeScale === 'month') {
        return date.date;
    }
    else if (timeScale === 'year') {
        return MonthEnum[date.month] + ' ' + date.date;
    }
}

// 
function HistoryChartTempHumiTime(props) {
    const style = historychartStyle();
    const dates = props.data.date.history;
    let temps = props.data.temp.history;
    let humis = props.data.humi.history;
    let times = props.data.predictTime.history;

    let data = [];
    for (let i = 0; i < dates.length; i++) {
        if(times[i] >= 0) {
            let date = GetDate(dates[i], props.timeScale);
            let hour = (dates[i].hour.length < 2)? '0'+dates[i].hour:dates[i].hour;
            let minute = (dates[i].minute.length < 2)? '0'+dates[i].minute:dates[i].minute;
            
            data.push({
                date: date + ' ' + hour + ':' + minute,
                temp: temps[i],
                humi: humis[i],
                time: times[i],
            });
        }
        
    }

    data.sort((a, b) => {
        return a.time - b.time;
    });

    return (
        <div className={style.root}>
            <ResponsiveContainer height={250} width='90%' className={style.container}>
                <LineChart data={data.slice()}>
                    <XAxis dataKey='time' unit={'hr'} label={{value: 'Predict Time', position: 'insideBottomRight', dy: 15, height: 10}}/>
                    <YAxis/>
                    <Tooltip cursor={{ strokeDasharray: '3 3' }}/>
                    <CartesianGrid strokeDasharray="1 1"/>
                    <Line type='monotone' dataKey='temp'  stroke={'#8884d8'}/>
                    <Line type='monotone' dataKey='humi'  stroke={'#82ca9d'}/>
                    <Legend height={36}/>
                </LineChart>
            </ResponsiveContainer>
        </div>
    );
}

function HistoryChartTempHumiError(props) {
    const style = historychartStyle();
    const dates = props.data.date.history;
    let temps = props.data.temp.history;
    let humis = props.data.humi.history;
    let actuals = props.data.actualTime.history;
    let predicts = props.data.predictTime.history;

    let data = [];
    for (let i = 0; i < dates.length; i++) {
        let date = GetDate(dates[i], props.timeScale);
        let hour = (dates[i].hour.length < 2)? '0'+dates[i].hour.length:dates[i].hour.length;
        let minute = (dates[i].minute.length < 2)? '0'+dates[i].minute.length:dates[i].minute.length;
        data.push({
            date: date + ' ' + hour + ':' + minute,
            temp: temps[i],
            humi: humis[i],
            error: predicts[i] - actuals[i],
        });
    }

    data.sort((a, b) => {
        return a.temp - b.temp;
    });

    return (
        <div className={style.root}>
            <ResponsiveContainer height={250} width='90%' className={style.container}>
                <ScatterChart>
                    <XAxis dataKey='temp' name='temp' unit={'\xb0c'} label={{value: 'temperature', position: 'insideBottomRight', dy: 15}}/>
                    <YAxis dataKey='humi' name='humi' unit='%' label={{value: 'humi', position: 'insideTopLeft', dy: -30}}/>
                    <ZAxis dataKey='error' name='error' range={[64, 144]}/>
                    <Tooltip cursor={{ strokeDasharray: '3 3' }}/>
                    <CartesianGrid strokeDasharray="1 1"/>
                    <Scatter name=' Predict Error' data={data.slice()} fill={'#FF5733'}/>
                    <Legend height={36}/>
                </ScatterChart>
            </ResponsiveContainer>
        </div>
    );
}

// define styles of content
const contentStyle = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    timescalebutton: {
        textAlign: 'center',
        paddingTop: '1.5em',
    },
    title: {
        textAlign: 'center',
        paddingTop: '1em',
        color: '#828282',
    },
    subtitle: {
        textAlign: 'center',
        paddingTop: '0.5em',
    },
    startbuttonDiv: {
        paddingTop: '2em',
        textAlign: 'center',
    }
}));

// content
function Content(props) {
    const style = contentStyle();
    const [timeScale, setTimeScale] = React.useState('week');

    // get data from server and update
    const GetData = async (type) => {
        props.ResetData();
        let temp = [];
        let humi = [];
        let predictTime = [];
        let actualTime = [];
        let energy = [];
        let date = [];
        const url = 'http://localhost:9000/getdata';
        await fetch(url, {
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: props.email,
                type: type,
            }),
            cache: 'no-cache',
            method: 'POST',
            mode: 'cors',
            redirect: 'manual'
        }).then((res) => {
            if(res.ok) {
                return res.json();
            }
        }).then((json) => {
            if(json.err) {
                console.error(json.err);
                return;
            }
            
            for (let i = 0; i < json.length; i++) {
                temp.push(parseFloat(json[i].temp));
                humi.push(parseInt(json[i].humi));
                predictTime.push(parseInt(json[i].predictTime));
                actualTime.push(parseInt(json[i].actualTime));
                energy.push(parseFloat(json[i].energy).toExponential(1));
                date.push(json[i].date);
            }
        }).catch((err) => {
            console.error(err);
        });
        props.UpdateHistoryData(temp, humi, predictTime, actualTime, energy, date);
    }

    const OnWeekButtonClicked = async (e) => {
        setTimeScale('week');
        await GetData('week');
    }

    const OnMonthButtonClicked = async (e) => {
        setTimeScale('month');
        await GetData('month');
    }

    const OnYearButtonClicked = async (e) => {
        setTimeScale('year');
        await GetData('year');
    }

    return (
        <div className={style.root}>
            {/* Title */}
            <div>
                <UI.Typography variant='h4' className={style.title}>
                    Data Overview
                </UI.Typography>
            </div>

            {/* Timescale changing buttons */}
            <div className={style.timescalebutton}>
                <UI.Button variant={timeScale==='week'?'contained':'outlined'} color='primary' size='small' onClick={OnWeekButtonClicked}>
                    Weekly
                </UI.Button>
                <UI.Button variant={timeScale==='month'?'contained':'outlined'} color='primary' size='small' onClick={OnMonthButtonClicked}>
                    Monthly
                </UI.Button>
                <UI.Button variant={timeScale==='year'?'contained':'outlined'} color='primary' size='small' onClick={OnYearButtonClicked}>
                    Year
                </UI.Button>
            </div>

            {/* line chart temp, humi and predict time */}
            <HistoryChartTempHumiTime
                data={props.data}
                timeScale={timeScale}
            />

            {/* line chart temp, humi and predict error */}
            <HistoryChartTempHumiError
                data={props.data}
                timeScale={timeScale}
            />
        </div>
    );
}

//  home page
export default class Data extends React.Component {
    // execute when home component is mounted
    async componentDidMount() {
        await this.GetData('week');
    }

    // get data from server and update
    async GetData(type) {
        this.props.ResetData();
        let temp = [];
        let humi = [];
        let predictTime = [];
        let actualTime = [];
        let energy = [];
        let date = [];
        const url = 'http://localhost:9000/getdata';
        await fetch(url, {
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: this.props.user.email,
                type: type,
            }),
            cache: 'no-cache',
            method: 'POST',
            mode: 'cors',
            redirect: 'manual'
        }).then((res) => {
            if(res.ok) {
                return res.json();
            }
        }).then((json) => {
            if(json.err) {
                console.error(json.err);
                return;
            }
            
            for (let i = 0; i < json.length; i++) {
                temp.push(parseFloat(json[i].temp));
                humi.push(parseInt(json[i].humi));
                predictTime.push(parseInt(json[i].predictTime));
                actualTime.push(parseInt(json[i].actualTime));
                energy.push(parseFloat(json[i].energy).toExponential(1));
                date.push(json[i].date);
            }
        }).catch((err) => {
            console.error(err);
        });
        this.props.UpdateHistoryData(temp, humi, predictTime, actualTime, energy, date);
    }

    render() {
        return (
            <div>
                <Content
                    email={this.props.user.email}
                    data={this.props.data}

                    UpdateHistoryData={this.props.UpdateHistoryData}
                    ResetData={this.props.ResetData}
                />
            </div>
        );
    }
}