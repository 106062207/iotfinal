import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import * as UI from '@material-ui/core/';
import {useHistory} from 'react-router-dom';

// style of content
const ContentStyle = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    title: {
        textAlign: 'center',
        paddingTop: '1.2em',
        color: '#828282',
    },
    formContent: {
        textAlign: 'center',
        paddingTop: '1.2em',
    },
    buttondiv: {
        paddingTop: '2em',
    },
    button: {
        color: '#828282',
    }
}));

// content component of account
function Content(props) {
    const style = ContentStyle();
    const [username, setusername] = React.useState('');
    const [email, setemail] = React.useState('');
    const [password, setpassword] = React.useState('');
    const [signinSignup, setSigninSignup] = React.useState(0);
    const history = useHistory();
    const text = (signinSignup === 0)? 'Sign In': 'Sign Up';
    const text2 = (signinSignup === 1)? 'Sign In': 'Sign Up';

    //function that update username
    const handleUsername = (event) => {
        setusername(event.target.value);
    };

    //function that update email
    const handleEmail = (event) => {
        setemail(event.target.value);
    };

    //function that update password
    const handlePassword = (event) => {
        setpassword(event.target.value);
    };

    //callback on button switch between signin/signup clicked
    const OnSwitchButtonClicked = (event) => {
        if (signinSignup === 0) {
            setSigninSignup(1);
        }
        else {
            setSigninSignup(0);
        }
    }

    //callback on form submitted
    const OnFormSubmitted = async (event) => {

        const route = (signinSignup === 0)? 'signin': 'signup';
        const url = 'http://localhost:9000/' + route;

        // async function that communicate with server
        await fetch(url, {
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                username: username,
                email: email,
                password: password
            }),
            cache: 'no-cache',
            method: 'POST',
            mode: 'cors',
            redirect: 'manual'
        }).then((res) => {
            if(res.ok) {
                return res.json();
            }
        }).then((json) => {
            if(json.err)
            {
                alert(json.err);
                return;
            }
            props.UpdateUser(json.user.username, json.user.email, json.user.password, json.signin);
        }).then(() => {
            // reset data to force anim update
            if (props.route !== '/') {
                props.ResetData();
                props.UpdateRoute('/');
            }
            // redirect to homepage
            history.push('/');
        }).catch((err) => {
            console.error(err);
        });
    };

    // form component that switch between signin / signup by variable signinSignup
    const Form = (
        <form className={style.root}>
            {/* Title switch between signin / signup*/}
            <UI.Typography variant='h2' className={style.title}>
                {text}
            </UI.Typography>
            {/* Input Form for signup/signin*/}
            <div className={style.formContent}>
                {signinSignup === 1 &&
                    <div>
                        <UI.TextField
                            margin='normal'
                            label='Username'
                            helperText='Some important text'
                            onChange={handleUsername}
                        />
                    </div>
                }
                <div>
                    <UI.TextField
                        margin='normal'
                        label='Email'
                        helperText='Some important text'
                        onChange={handleEmail}
                    />
                </div>
                <div>
                    <UI.TextField
                        margin='normal'
                        label='Password'
                        helperText='Some important text'
                        onChange={handlePassword}
                    />
                </div>
                <div className={style.buttondiv}>
                    <UI.Button onClick={OnFormSubmitted} variant='contained' color='primary' size='large'>
                        {text}
                    </UI.Button>
                    <UI.Button onClick={OnSwitchButtonClicked} variant='outlined' size='large' className={style.button}>
                        {text2}
                    </UI.Button>
                </div>
            </div>
        </form>
    );

    return (
        <div>
            {Form}
        </div>
    );
}

// main component of account page
export default class Account extends React.Component {
    render() {
        return (
            <div>
                <Content
                    route={this.props.route}
                    UpdateRoute={this.props.UpdateRoute}
                    UpdateUser={this.props.UpdateUser}
                    ResetData={this.props.ResetData}
                />
            </div>
        )
    }
}