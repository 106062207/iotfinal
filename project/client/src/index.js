import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import Home from './home';
import Account from './account';
import Navbar from './navbar';
import Data from './data';

// uppest parent that maintains global variables and routes
class Main extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            route: '/',
            data: {
                temp: {
                    current: 0.0,
                    history: [],
                },
                humi: {
                    current: 0.0,
                    history: [],
                },
                predictTime: {
                    current: 0.0,
                    history: [],
                },
                actualTime: {
                    current: 0.0,
                    history: [],
                },
                energy: {
                    current: 0.0,
                    history: [],
                },
                date: {
                    current: {},
                    history: [],
                }
            },
            user: {
                username: '',
                email: '',
                password: '',
                signin: false,
            },
            start: false,
        };

        this.UpdateRoute = this.UpdateRoute.bind(this);
        this.ResetData = this.ResetData.bind(this);
        this.UpdateCurrentData = this.UpdateCurrentData.bind(this);
        this.UpdateHistoryData = this.UpdateHistoryData.bind(this);
        this.UpdateUser = this.UpdateUser.bind(this);
        this.UpdateStart = this.UpdateStart.bind(this);
    }


    // maintain current route
    UpdateRoute(route) {
        this.setState({route: route});
    }

    // function to pass to child that resets data (temp, humi) to re-render
    ResetData() {
        console.log('reset');
        let data = this.state.data;
        data.temp = {
            current: 0.0,
            history: [],
        };
        data.humi = {
            current: 0.0,
            history: [],
        };
        this.setState({data: data});
    }

    // function to pass to child that updates current data
    UpdateCurrentData(temp, humi, predictTime, actualTime, date) {
        let data = this.state.data;
        data.temp.current = temp;
        data.humi.current = humi;
        data.predictTime.current = predictTime;
        data.actualTime.current = actualTime;
        data.date.current = date;
        this.setState({data: data});
    }

    // function to pass to child that updates history data
    UpdateHistoryData(temp, humi, predictTime, actualTime, energy, date) {
        let data = this.state.data;
        data.temp.history = temp;
        data.humi.history = humi;
        data.predictTime.history = predictTime;
        data.actualTime.history = actualTime;
        data.energy.history = energy;
        data.date.history = date;
        this.setState({data: data});
    }

    // function to pass to child that updates user
    UpdateUser(username, email, password, signin) {
        const user = {
            username: username,
            email: email,
            password: password,
            signin: signin,
        };

        this.setState({user: user});
    }

    // update start of drying process
    UpdateStart() {
        this.setState({start: !this.state.start});
    }

    render() {
        return (
            <div>
                {/* Switch different components by routes */}
                <Switch>
                    {/* sign in / sign up page */}
                    <Route path='/account'>
                        <div>
                            <Navbar
                                route={this.state.route}
                                user={this.state.user}

                                UpdateRoute={this.UpdateRoute}
                                ResetData={this.ResetData}
                            />
                            <Account
                                route={this.state.route}

                                UpdateRoute={this.UpdateRoute}
                                ResetData={this.ResetData}
                                UpdateUser={this.UpdateUser}
                            />
                        </div>
                        
                    </Route>
                    {/* data page */}
                    <Route path='/data'>
                        <div>
                            <Navbar
                                route={this.state.route}
                                user={this.state.user}

                                UpdateRoute={this.UpdateRoute}
                                ResetData={this.ResetData}
                            />
                            <Data
                                route={this.state.route}
                                data={this.state.data}
                                user={this.state.user}

                                UpdateHistoryData={this.UpdateHistoryData}
                                ResetData={this.ResetData}
                            />
                        </div>
                    </Route>
                    {/* home page */}
                    <Route path='/'>
                        <div>
                            <Navbar
                                route={this.state.route}
                                user={this.state.user}

                                UpdateRoute={this.UpdateRoute}
                                ResetData={this.ResetData}
                            />
                            <Home
                                route={this.state.route}
                                user={this.state.user}
                                data={this.state.data}
                                start={this.state.start}

                                UpdateRoute={this.UpdateRoute}
                                UpdateCurrentData={this.UpdateCurrentData}
                                UpdateStart={this.UpdateStart}
                                ResetData={this.ResetData}
                            />
                        </div>
                    </Route>
                </Switch>
                
            </div>
        );
    }
}

// render in root of html
ReactDOM.render(
    <Router>
        <Main/>
    </Router>,
    document.querySelector('#root')
);